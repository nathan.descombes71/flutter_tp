
import 'package:flutter_tp/domain/entities/task.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto.dart';

class TaskFactory{
  Task fromDto(TaskDto dto){
    return Task(id: dto.id,
                title: dto.title,
                desc: dto.desc,
                level: dto.level);
  }

  TaskDto toDto(Task entity){
    return TaskDto(id: entity.id,
                   title: entity.title,
                   desc: entity.desc,
                   level: entity.level);
  }
}