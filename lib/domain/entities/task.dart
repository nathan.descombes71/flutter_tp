class Task {
  int id;
  String title;
  String desc;
  int level;

  Task({
    this.id,
    this.title,
    this.desc,
    this.level,
  });
}

