import 'package:flutter/material.dart';
import 'package:flutter_tp/domain/entities/task.dart';
import 'package:flutter_tp/domain/entities/task_factory.dart';
import 'package:flutter_tp/domain/repositories/task_repositories.dart';
import 'package:flutter_tp/infrastructure/api/rest_api_response.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto.dart';

class TaskController{
  final TaskFactory entityFactory;
  final TaskRepositories repo;

  TaskController({
    this.repo, 
    @required this.entityFactory
 
  }): super();
  
   Future<RestApiResponse<List<Task>>> getAll() async {
    try {
      var result = await repo.getAll();

      var response= RestApiResponse<List<Task>>(message: result.message, data: result.data.map<Task>((e) => entityFactory.fromDto(e)).toList());

      return response;
  
    } catch (e) {
      return null;
    }
  }
}
