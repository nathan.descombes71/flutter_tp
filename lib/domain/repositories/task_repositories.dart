

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tp/infrastructure/api/rest_api_response.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto_factory.dart';

abstract class TaskRepositories {
 TaskRepositories(
      {@required Dio client,
      @required TaskDtoFactory dtoFactory,
      @required String controller});


    Future<RestApiResponse<List<TaskDto>>> getAll();
}