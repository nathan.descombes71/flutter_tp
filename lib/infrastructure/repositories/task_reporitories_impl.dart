import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tp/domain/repositories/task_repositories.dart';
import 'package:flutter_tp/infrastructure/api/rest_api_response.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto_factory.dart';

class TaskReporitoriesImpl extends TaskRepositories {
  TaskReporitoriesImpl({
    @required Dio client,
    @required TaskDtoFactory dtoFactory,
  }) : super(client: client, dtoFactory: dtoFactory, controller: "/task",);

  //Normalement

  //   Future<RestApiResponse<List<TaskDto>>> getAll({
  //   Map<String, dynamic> queryParameters,
  // }) async {
  //   try {
  //     var response = await client.get(
  //       '$controller',
  //       queryParameters: queryParameters,
  //     );

  //     return RestApiResponse<List<TaskDto>>(
  //       message: response.data['message'],
  //       data: response.data['data'].map<D>((e) {
  //         return dtoFactory.fromJson(e);
  //       }).toList(),
  //     );
  //   } catch (e) {
  //     throw (e);
  //   }
  // }


//Mais dans cette exercice : 
  
  @override
  Future<RestApiResponse<List<TaskDto>>> getAll() async {
    List<TaskDto> response = [
      TaskDto(id: 1, title: "task1", desc: "desc 1", level: 1), 
      TaskDto(id: 2, title: "task2", desc: "desc 2", level: 2),
      TaskDto(id: 3, title: "task3", desc: "desc 3", level: 2),
      TaskDto(id: 4, title: "task4", desc: "desc 4", level: 3)
      ];

      return RestApiResponse<List<TaskDto>>(message: "Recu avec succés !", data: response);
  }
}
