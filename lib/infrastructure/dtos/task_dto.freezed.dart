// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'task_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
TaskDto _$TaskDtoFromJson(Map<String, dynamic> json) {
  return _TaskDto.fromJson(json);
}

/// @nodoc
class _$TaskDtoTearOff {
  const _$TaskDtoTearOff();

// ignore: unused_element
  _TaskDto call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'title') String title,
      @JsonKey(name: 'desc') String desc,
      @JsonKey(name: 'level') int level}) {
    return _TaskDto(
      id: id,
      title: title,
      desc: desc,
      level: level,
    );
  }

// ignore: unused_element
  TaskDto fromJson(Map<String, Object> json) {
    return TaskDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $TaskDto = _$TaskDtoTearOff();

/// @nodoc
mixin _$TaskDto {
  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'title')
  String get title;
  @JsonKey(name: 'desc')
  String get desc;
  @JsonKey(name: 'level')
  int get level;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $TaskDtoCopyWith<TaskDto> get copyWith;
}

/// @nodoc
abstract class $TaskDtoCopyWith<$Res> {
  factory $TaskDtoCopyWith(TaskDto value, $Res Function(TaskDto) then) =
      _$TaskDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'title') String title,
      @JsonKey(name: 'desc') String desc,
      @JsonKey(name: 'level') int level});
}

/// @nodoc
class _$TaskDtoCopyWithImpl<$Res> implements $TaskDtoCopyWith<$Res> {
  _$TaskDtoCopyWithImpl(this._value, this._then);

  final TaskDto _value;
  // ignore: unused_field
  final $Res Function(TaskDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object desc = freezed,
    Object level = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      title: title == freezed ? _value.title : title as String,
      desc: desc == freezed ? _value.desc : desc as String,
      level: level == freezed ? _value.level : level as int,
    ));
  }
}

/// @nodoc
abstract class _$TaskDtoCopyWith<$Res> implements $TaskDtoCopyWith<$Res> {
  factory _$TaskDtoCopyWith(_TaskDto value, $Res Function(_TaskDto) then) =
      __$TaskDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'title') String title,
      @JsonKey(name: 'desc') String desc,
      @JsonKey(name: 'level') int level});
}

/// @nodoc
class __$TaskDtoCopyWithImpl<$Res> extends _$TaskDtoCopyWithImpl<$Res>
    implements _$TaskDtoCopyWith<$Res> {
  __$TaskDtoCopyWithImpl(_TaskDto _value, $Res Function(_TaskDto) _then)
      : super(_value, (v) => _then(v as _TaskDto));

  @override
  _TaskDto get _value => super._value as _TaskDto;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object desc = freezed,
    Object level = freezed,
  }) {
    return _then(_TaskDto(
      id: id == freezed ? _value.id : id as int,
      title: title == freezed ? _value.title : title as String,
      desc: desc == freezed ? _value.desc : desc as String,
      level: level == freezed ? _value.level : level as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_TaskDto implements _TaskDto {
  const _$_TaskDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'title') this.title,
      @JsonKey(name: 'desc') this.desc,
      @JsonKey(name: 'level') this.level});

  factory _$_TaskDto.fromJson(Map<String, dynamic> json) =>
      _$_$_TaskDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'title')
  final String title;
  @override
  @JsonKey(name: 'desc')
  final String desc;
  @override
  @JsonKey(name: 'level')
  final int level;

  @override
  String toString() {
    return 'TaskDto(id: $id, title: $title, desc: $desc, level: $level)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TaskDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.desc, desc) ||
                const DeepCollectionEquality().equals(other.desc, desc)) &&
            (identical(other.level, level) ||
                const DeepCollectionEquality().equals(other.level, level)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(desc) ^
      const DeepCollectionEquality().hash(level);

  @JsonKey(ignore: true)
  @override
  _$TaskDtoCopyWith<_TaskDto> get copyWith =>
      __$TaskDtoCopyWithImpl<_TaskDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_TaskDtoToJson(this);
  }
}

abstract class _TaskDto implements TaskDto {
  const factory _TaskDto(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'title') String title,
      @JsonKey(name: 'desc') String desc,
      @JsonKey(name: 'level') int level}) = _$_TaskDto;

  factory _TaskDto.fromJson(Map<String, dynamic> json) = _$_TaskDto.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'title')
  String get title;
  @override
  @JsonKey(name: 'desc')
  String get desc;
  @override
  @JsonKey(name: 'level')
  int get level;
  @override
  @JsonKey(ignore: true)
  _$TaskDtoCopyWith<_TaskDto> get copyWith;
}
