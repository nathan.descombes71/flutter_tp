// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TaskDto _$_$_TaskDtoFromJson(Map<String, dynamic> json) {
  return _$_TaskDto(
    id: json['id'] as int,
    title: json['title'] as String,
    desc: json['desc'] as String,
    level: json['level'] as int,
  );
}

Map<String, dynamic> _$_$_TaskDtoToJson(_$_TaskDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'desc': instance.desc,
      'level': instance.level,
    };
