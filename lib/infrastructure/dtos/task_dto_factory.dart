
import 'abstract/dto_factory.dart';
import 'task_dto.dart';

class TaskDtoFactory implements DtoFactory<TaskDto> {
  @override
  TaskDto fromJson(Map<String, dynamic> json) =>
      TaskDto.fromJson(json);

  @override
  Map<String, dynamic> toJson(TaskDto dto) => dto.toJson();
}