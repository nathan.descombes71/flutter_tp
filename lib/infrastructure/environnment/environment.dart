import 'package:get/get.dart';

import 'package:flutter_tp/infrastructure/environnment/env_type.dart';

class Env extends GetxService {
  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ Environment                                                     ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  EnvType kEnv = EnvType.dev();

  String kDefaultUsername = "";
  String kDefaultPassword = "";

  bool get showDebugBanner => kEnv != EnvType.prod();

  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ AppKeys                                                         ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  String get kAppKey {
    return kEnv.when(
      dev: () => "ExempleAppKey",
      rd: () => "ExempleAppKeyRD",
      rc: () => "ExempleAppKeyRC",
      prod: () => "ExempleAppKeyProd",
    );
  }

  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ Base Urls                                                       ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  String get kBaseUrl {
    return kEnv.when(
      dev: () => "ExampleURLApi",
      rc: () => "ExampleURLApi",
      rd: () => "ExampleURLApi",
      prod: () => "ExampleURLApi",
    );
  }

  String get kAuthBaseUrl {
    return kEnv.when(
      dev: () => "ExampleURLApiPortail",
      rc: () => "ExampleURLApiPortail",
      rd: () => "ExampleURLApiPortail",
      prod: () => "ExampleURLApiPortail",
    );
  }
}
