
import 'package:dio/dio.dart';
import 'package:flutter_tp/infrastructure/environnment/environment.dart';
import 'package:get/get.dart';


class RestApiClient {
  final Dio client = Dio();

  RestApiClient() {
    client.options.baseUrl = Get.find<Env>().kBaseUrl;
    client.options.receiveTimeout = 15000;
    client.options.connectTimeout = 15000;
  }
}
