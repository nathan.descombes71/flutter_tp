import 'package:flutter/material.dart';
import 'package:flutter_tp/presentation/views/main_views/main_controller.dart';
import 'package:flutter_tp/presentation/views/main_views/main_views_widget/card_task.dart';
import 'package:get/get.dart';


class MainView extends GetView<MainController> {
  MainView({Key key}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.state.value.when(
        initial: (_) => SizedBox(),
        loading: (_) => SizedBox(),
        loaded: (_) => _buildContent(),
        error: (type, message) {
          return Text(
            message
          );
        },
      ),
    );
  }


  Widget _buildContent() {
    return Scaffold(
      appBar: AppBar(),
      body: SizedBox(width: 400, 
        child: ListView.builder(
          itemCount: controller.allTask.length, 
          itemBuilder: (BuildContext context, int index) 
            {
              return CardTask(task: controller.allTask[index],);
            },
          ),
        )

    );
  }
}
