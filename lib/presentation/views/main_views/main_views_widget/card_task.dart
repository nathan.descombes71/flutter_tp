import 'package:flutter/material.dart';
import 'package:flutter_tp/domain/entities/task.dart';

class CardTask extends StatelessWidget {
  final Task task;

   CardTask({
    @required this.task});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      color: task.level == 1 ? Colors.green : task.level == 2 ? Colors.orange : task.level == 3 ? Colors.red: Colors.grey,
      child: Container(
        // height: 200,
        child: Column(
          // mainAxisAlignment: .center,
          children:[
            SizedBox(height: 20),
            Text(task.title, style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold,),),
            
            Container(color: Colors.black, height: 5,margin: EdgeInsets.only(top: 10, bottom: 10, left: 50, right: 50),),

            Text(task.desc, style: TextStyle(fontStyle: FontStyle.italic)),

            SizedBox(height: 20),
          ]
        ),
      ),
    );
  }
}
