

import 'package:flutter_tp/domain/controllers/task_controller.dart';
import 'package:flutter_tp/domain/entities/task.dart';
import 'package:flutter_tp/domain/entities/task_factory.dart';
import 'package:flutter_tp/domain/repositories/task_repositories.dart';
import 'package:flutter_tp/infrastructure/api/rest_api_client.dart';
import 'package:flutter_tp/infrastructure/dtos/task_dto_factory.dart';
import 'package:flutter_tp/infrastructure/repositories/task_reporitories_impl.dart';
import 'package:get/get.dart';

import 'main_controller.dart';

class MainControllerBindings extends Bindings {
  @override
  void dependencies() {
    /*************************************/
    //                Leaves              /
    /*************************************/

    Get.lazyPut<TaskFactory>(() => TaskFactory());

    Get.lazyPut<TaskDtoFactory>(() => TaskDtoFactory());

 Get.lazyPut(
      () => TaskReporitoriesImpl(
          client: Get.find<RestApiClient>().client,
          dtoFactory: Get.find<TaskDtoFactory>()),
    );

    Get.lazyPut(() => TaskController(
          entityFactory: Get.find<TaskFactory>(),
          repo: Get.put(
            TaskReporitoriesImpl(client: Get.find<RestApiClient>().client, dtoFactory: Get.find<TaskDtoFactory>()),
          ),
        ));


   

    //****************************************//

    Get.lazyPut<MainController>(
      () => MainController(
        controllerTask: Get.put(
          TaskController(
            entityFactory: Get.put<TaskFactory>(TaskFactory()),
            repo: Get.put<TaskRepositories>(
              TaskReporitoriesImpl(client: Get.find<RestApiClient>().client, dtoFactory: Get.find<TaskDtoFactory>()),
            ),
            // authRepository: Get.find<AuthRepository>(),
          ),
        ),
      ),
    );
  }
}
