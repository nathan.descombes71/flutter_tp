
import 'package:flutter_tp/presentation/core/error_type.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'main_state.freezed.dart';

@freezed
abstract class MainState with _$MainState {
  const factory MainState.initial({String message}) = Initial;
  const factory MainState.loading({String message}) = Loading;
  const factory MainState.loaded({String message}) = Loaded;
  const factory MainState.error({ErrorType type, String message}) = Error;
}
