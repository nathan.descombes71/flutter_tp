
import 'package:flutter/material.dart';
import 'package:flutter_tp/domain/controllers/task_controller.dart';
import 'package:flutter_tp/domain/entities/task.dart';
import 'package:flutter_tp/presentation/views/main_views/main_state.dart';
import 'package:get/get.dart';


class MainController extends GetxController {
  final TaskController controllerTask;

  MainController({
    @required this.controllerTask,
  });
  
Rx<MainState> state = MainState.initial().obs;
  RxBool extended = false.obs;


  List<Task> allTask = [];

  @override
  Future<void> onInit() async {
     try{
       state.value = MainState.loading();
    allTask = (await controllerTask.getAll()).data;
      print("données");
       state.value = MainState.loaded();
      } catch (e) {
        state.value = MainState.error(message: e);
      throw (e);
    }
    super.onInit();
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
