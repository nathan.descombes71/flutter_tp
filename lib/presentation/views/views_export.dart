// Export Main view
export 'package:flutter_tp/presentation/views/main_views/main_bindings.dart';
export 'package:flutter_tp/presentation/views/main_views/main_controller.dart';
export 'package:flutter_tp/presentation/views/main_views/main_view.dart';