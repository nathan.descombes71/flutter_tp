

class Routes {
  static Future<String> get initialRoute async {
   
    return MAIN;
  }

  static const MAIN = '/main';
}
