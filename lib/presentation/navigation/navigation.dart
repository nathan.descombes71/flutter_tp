import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:flutter_tp/presentation/views/views_export.dart' as mobile;

import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    // REVIEW Mobile only routes
    GetPage(
      name: Routes.MAIN,
      page: () => mobile.MainView(),
      binding: mobile.MainControllerBindings(),
    ),
   
  ];
}
