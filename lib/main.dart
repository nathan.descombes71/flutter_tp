
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'infrastructure/api/rest_api_client.dart';
import 'infrastructure/environnment/environment.dart';
import 'presentation/navigation/navigation.dart';
import 'presentation/navigation/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  var initialRoute = await Routes.initialRoute;
  runApp(
    GetMaterialApp(
      title: 'XEFI Congés',
      initialRoute: initialRoute,
      getPages: Nav.routes,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.dark,
      locale: Get.deviceLocale,
      supportedLocales: const <Locale>[
        Locale('fr', ''),
        Locale('en', ''),
      ],
    ),
  );
}

Future<void> initServices() async {
  print('starting services ...');

  /// Here is where you put get_storage, hive, shared_pref initialization.
  /// or moor connection, or whatever that's async.
  Get.lazyPut(() => Env());
  // Get.lazyPut(() => User());

  Get.lazyPut(
      () => RestApiClient());
  // Get.lazyPut(
  //     () => AuthApiClient(authApiInterceptor: Get.put(AuthApiInterceptor())));

  print('All services started...');
}
